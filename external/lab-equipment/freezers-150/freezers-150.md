---
layout: page
permalink: /external/lab-equipment/freezers-150/
shortcut: lab:freezers-150
redirect_from:
  - /cards/lab-equipment:freezers-150
  - /external/cards/lab-equipment:freezers-150
  - /lab/freezers-150
  - /external/external/lab-equipment/freezers-150/
  - /cards/lab:freezers-150
  - /external/cards/lab:freezers-150
---
# Maintenance of -150°C freezers

Routine maintenance of fridges and freezers prevents from issues and preserves integrity of samples.

In the event of a fridge or a freezer would get damaged or have a failure due to a lake of maintenance on the user side, the cost of the repair or of the replacement will be charged to the group in charge of the device.

The fridges and freezers are under constant temperature monitoring. In order to avoid alarms, the monitoring has to be disconnected before the cleaning of 4°C fridges and the defrosting of -20°C/-80°C freezers (see [How to deactivate the alarm in Sensor4Lab](#how-to-deactivate-the-alarm-in-sensor4lab)). 

**Please inform the Instrument Care team before every kind of maintenance by sending [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=6ea39fdbdbec38105c72ef3c0b96196a&sysparm_category=af924f17dbac38105c72ef3c0b96194b) 24h before starting your maintenance**.

Always start your maintenance in the morning. 

When the maintenance is finished and the temperature is back to normal, reply on the open ticket to let them know you are done.


## How to deactivate the alarm in Sensor4Lab

All the freezers are monitored via a software called Sensor4Lab. To avoid any alarm due to the defrosting, the first step of the defrosting process is always to put the monitoring on hold. This step is needed for the cleaning of 4°C fridges and defrosting of -20°C/-80°C freezers.

**Only technicians have access to Sensor4lab**. Please ask a technician of your team to disable the sensor associated to the fridge you want to clean or defrost. 
-	If you’re a technician, go on [Sensor4lab](https://sensor4lab.lcsb.uni.lu/account/logon) and login with your credentials received from the Instrument Care Team. If you don’t have any credentials yet, please send [a ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=dc205fd7dbec38105c72ef3c0b96191d&sysparm_category=af924f17dbac38105c72ef3c0b96194b) 
-	Please follow the process explained [here](https://dutycall.lcsb.uni.lu/cards/alarm-management/deactivate-alarm/)

Please note that there is a 30 minutes delay between the moment that you disable the sensor and the response of the system. This means that you should disable the monitoring 30 min before you start the maintenance.

## How to defrost a -150C freezer

This section applies to the -150°C freezer (Sanyo MDF-C2156VAN, BT1-407)

<div align="center">
<img src="img/img16.png" width="200">
</div>

#### Inner doors defrosting - once a month

- The frost forming on the inner door can be removed using the scraper
- Dry the inner door and the edges with a cloth before closing back the door.
- Inform the Instrument Care team that you are finished and that everything is back to normal
- Fill the maintenance form located on the door of the freezer

#### Condenser filter cleaning - once a month

-	Open the grille by pulling it to you
-	Take out the condenser filter and wash it with water
-   Don’t touch the condenser directly! This may cause injury by hot surface  
-	Replace the condenser filter and the grille
-	Check that the filter check lamp is off
-	Fill the maintenance form located on the door of the freezer 
