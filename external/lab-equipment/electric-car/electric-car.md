---
layout: page
permalink: /external/lab-equipment/electric-car/
shortcut: lab:electric-car
redirect_from:
  - /cards/lab-equipment:electric-car
  - /external/cards/lab-equipment:electric-car
  - /lab/electric-car
  - /external/external/lab-equipment/electric-car/
  - /cards/lab:electric-car
  - /external/cards/lab:electric-car
---


# LCSB Electric Car

The LCSB electrical car enables to connect the different buildings of LCSB.

It is dedicated to the transport of samples, goods, materials and equipment from one LCSB building to another or from a sampling site. It can also be used to go to BTA for a maintenance, a repair or a short time experiment or meeting.

The use of the LCSB electic car is **NOT for personal use**. For personal transportation please consider first the public transportation system, it is fast and efficient.
2 bus connect Belval to BTA (see map of Belval)
- [ligne 7](https://www.mobiliteit.lu/wp-content/uploads/horaires-new/tice/7.pdf?v=20230915) take the bus in front of the Rockhal (1) or in front of MNO (Porte des Sciences) (2) and exit in Rue Henri Koch (3).
- [ligne 750](https://www.mobiliteit.lu/wp-content/uploads/horaires-new/rgtr/750.pdf?v=20231009) take the bus in front of MNO (Porte des Sciences) (2) or at "Porte de france" (4) and exit at "Lycée Guillaume Kroll" (5) .

<div align="center">
<img src="img/img6.png" width="70%">
</div>

## Authorisation to use the electrical car and training

Send an email to the Instrument Care Team to inform them that you would like to use the LCSB car. You will be contacted by the person in charge of giving the training and you will receive the *Car convention* that you will have to read and returned signed to ICT.

## Charging the electric car


<div align="center">
<img src="img/img1.png" width="600">
</div>

1. Specific wall socket or recharging terminal
2. Electric charging connection
3. Charging cord

   

- The car and the charging station are located in -2 BT2, on the parking spots 63 or 64 (Luxembourgish licence plate: FL4979)
- The car is plugged each Friday and unplugged by the first person to use it the following week
- The autonomy of the car when fully charged is approximately 100km
- The car has to be charged when the autonomy reaches 50km

### Connect the car to the charging station

- Check that the charging station is ON. If it is not the case, contact the instrument care team [instrument-care@uni.lu](mailto:instrument-care@uni.lu)

<div align="center">
<img src="img/img2.png" width="300">
</div>

- Turn on the car to load the propulsion battery

- Put the gear shift in P mode.

<div align="center">
<img src="img/img3.png" width="300">
</div>

- The engine should be OFF

- Take the charging cable from the trunk of the car

**DO NOT add a prolongation cable or another type of cable!!!**

- Slide up the cover of the domestic cupboard and connect the cable

<div align="center">
<img src="img/img4.png" width="300">
</div>

- Plug the cable to the car

**The cable cannot be plugged if the car is closed. Leave the car open.**

**Make sure to unroll the charging cord to avoid overheating.**

- Open the charging hatch

<div align="center">
<img src="img/img5.png" width="600">
</div>

- Open the valve

- Plug the cord using the cord handle

Once the cord is plugged, the sign below will appear on the dashboard. The cord is automatically locked to the car.

<div align="center">
<img src="img/img.png" width="100">
</div>

The beginning of the carge is signalled by 5 blinking of the hazard lights. A message on the dashboard indicates the duration of the charge.

3 hours are necessary to charge the car for 50km.

- Lock the car using the car key

### Disconnect the car from the charging station

- Unlock the car

- From the moment the car is unlocked, you have 30 seconds before the cable is locked again to the car

- Use the cable handle to remove it from the car (the cable might be hot)

- Once the cable is unplugged, the sign below should disappear from the dashboard

<div align="center">
<img src="img/img.png" width="100">
</div>

- Close back the valve and the charging hatch

- Unplug the cable from the charing station

- Put the cable back in the trunk of the car

- Lock the car and lock the charging station





