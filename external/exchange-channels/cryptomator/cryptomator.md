---
card_order: 500
layout: page
permalink: /external/exchange-channels/cryptomator/
shortcut: exchange-channels:cryptomator
redirect_from:
  - /cards/exchange-channels:cryptomator
  - /external/cards/exchange-channels:cryptomator
  - /exchange-channels/cryptomator
  - /external/external/exchange-channels/cryptomator/
---
# Cryptomator

Cryptomator is a cloud cryptor - a tool allowing to work on and to share encrypted data seamlessly. The main benefit of using Cryptomator is that the service is transparent - after proper setup, the user and all applications can work with the data exactly the same way as before while the data is more secured.

<img src="img/cryptomator-diagram.png" height="500px"><br>

Cryptomator protects your data by ingesting it in a so-called vault, which is mounted to a virtual drive and password protected. To access and view the data, you must unlock the vault with the created password. The data is stored on the cloud of your choice when creating the vault, but is encrypted automatically by the Cryptomator client. It is only from the virtual drive which act as a USB flash drive that you can access and view the data, as it shows the decrypted view of its content. Ingesting your files directly into the virtual drive permits Cryptomator to automatically encrypt the data with AES and 256-bit key length. 

Please follow the below sections to: download Cryptomator locally on your computer, enable Cryptomator on your preffered cloud, or manage vaults as an administrator. Please visit Cryptomator’s official website for the full documentation on [vault management](https://docs.cryptomator.org/en/latest/). 

`Note:` Cryptomator does not offer cloud storage but adds an extra protection to your data stored on the cloud by automatically encrypting the files in the vault. 

## Downloading Cryptomator Desktop
The desktop version of Cryptomator is available both for macOS, Windows, and Linux. Please follow the [guide](https://docs.cryptomator.org/en/latest/desktop/setup/) on Cryptomator's official website to install the application on your operating system. 

 <img src="img/cryptomator-main.png" height="400px"><br>

## Setting up key management system with Cryptomator Hub
 Cryptomator Hub adds key and access management to your vaults from a central component deployed on your infrastructure, permitting collaborative work. As an admin, you can share your vaults with other people, who can access the vault by authenticating with Keycloak. There is thus no need for creating new accounts nor sharing encryption passwords with collaborators which are easily stolen, copied, or misused, and no data is shared on any online services. 

As a vault administrator it is important to manage the admin password properly. In case you lose or forget the admin password, a recovery key is needed to restore access to the vault. This recovery key is generated upon creating the vault (see guide below). In practical, it is the responsibility of the project manager and/or PI to manage and store the admin vault password securely. Please use passphrase and proper password management when creating the admin vault password. For further information, visit the How-to Card on [password management]( https://howto.lcsb.uni.lu/?access:passwords). 

## Enabling Cryptomator using a cloud of your choice
1. Go to the LCSB instance of Cryptomator Hub https://lcsb.cryptomator.cloud/.
2. Click on “LCSB Login”. 

    <img src="img/cryptomatorHub-login.png" height="400px"> <br>

3. Select "uni.lu" account to sign in. 

    <img src="img/uni-login.png" height="400px"> <br>

4. When signed in successfully, you will be presented with the Hub's main page. To create a new vault, click the “Add” button on the top left corner and then "create new". 

    <img src="img/hub-mainpage.png" height="350px"> <br>

5. Fill out vault name and admin password. The admin password is required to grant or revoke access to the vault. When the form is completed, click “Next”. 

    <img src="img/creating-vault.png" height="350px"> <br>

6. A recovery key is displayed on the page. Copy the recovery key and store it in a safe location (e.g. password management system). The recocery key is needed for restoring access to the vault data. Tick the checkbox and click “Create Vault”. 

    <img src="img/recovery-key.png" height="350px"> <br>

7. The vault has now been successfully created from the Hub. Click “download zipped vault folder”. 

    <img src="img/download-vault-template.png" height="350px"> <br>

8. Unzip the vault in your preferred cloud storage location (e.g., ownCloud or OneDrive). 

    <img src="img/ownCloud-folder.png" height="300px"> <br>

## Managing a vault as an administrator
The following steps can only be performed by the vault administrator. 

1. From the Hub's main page, click on the vault you wish to share and then "Manage vault". 

    <img src="img/manage-vault.png" height="250px"> <br>
 
2. Enter the vault admin password and click "Manage". 

    <img src="img/vault-admin-password.png" height="200px"> <br>
 
3. You are now able to share the vault with other team members by simply searching for the user and clicking the “add” button.

    <img src="img/add-member.png" height="450px"> <br> 
 
4. The team member has to register their local device to the shared vault (see next section). Once the device of the user has been registered to the vault, the admin can grant access by clicking on “update permission”. 

    <img src="img/grant-access.png" height="150px"> <br>

5. Once access is granted, the team member can unlock the vault locally. 

## Collaborating on a shared vault as a user
1. Open the desktop application. Click the “add’ button and then “Existing vault”. 

    <img src="img/add-existing-vault.png" height="400px"> <br> 

2. Click on “Choose”.

    <img src="img/vault-file.png" height="400px"> <br> 
 
3. Go to the shared vault folder on the cloud. Select "vault.cryptomator" and then "Open".  

    <img src="img/import-vault.png" height="400px"> <br> 

4. Click "Unlock now".

     <img src="img/add-vault.png" height="350px"> <br> 

5. Cryptomator will automatically redirect you to the login page. Please authenticate yourself with Keycloak.

6. If it is the first time you connect your device to the shared vault, you have to register the device. In this case, you will be prompt to enter a device name and then click "Confirm. The admin can now grant access to the device. 

   <img src="img/register-device.png" height="200px"> <br> 

7. Once the admin grants access the vault is unlocked. Click “Reveal drive” to access the vault on the virtual drive. 

   <img src="img/unlock-vault.png" height="400px"> <br> 

8. All data can now be ingested in the vault on the virtual drive.

    <img src="img/vault-data.png" height="400px"> <br> 

9.	The cloud cannot see anything in the folder, as all data is encrypted - both folder/file name and files.

    <img src="img/cloud-data.png" height="400px"> <br> 

`Note:` Always ingest the files directly into the vault on the virtual drive, as it will be encrypted and stored properly. If the file is ingested directly in the folder of the cloud storage location, it will not get encrypted by Cryptomator! 





