---
layout: page
permalink: /external/lab-hsa/spill-bsc/
shortcut: lab:spill-bsc
redirect_from:
  - /cards/lab-hsa:spill-bsc
  - /external/cards/lab-hsa:spill-bsc
  - /lab/spill-bsc
  - /external/external/lab-hsa/spill-bsc/
  - /cards/lab:spill-bsc
  - /external/cards/lab:spill-bsc
---
# How to deal with a spill in a BSC

## Minor spill in a BSC (few milliliters of hazardous biological material inside a BSC)

The main challenge in this case is to keep benefiting from the protection offered by the BSC while avoiding to export any contamination outside the cabinet.

-   Stop activities. Keep the BSC running and keep applying the rules for safe work in a BSC.
-   If your gloves have been exposed to contamination, change gloves. Remove the gloves carefully without spreading the contamination.
-   Close all open containers.
-   Spray gently the spilled area and its surroundings with 70% ethanol or other disinfectant depending on the biological agent in use.
-   Wipe the work surface and sprayed objects with some absorbing paper.
-   Check for the cleanness of underneath part of the cabinet working area. Repeat the gentle spraying step if needed and wipe.
-   Discard the absorbing paper, gloves and everything that needs to be eliminated in the infectious waste.
-   Put on new gloves.
-   Proceed to a routine decontamination of the BSC using 70% ethanol or the disinfectant that is used for routine decontamination.
-   Resume or terminate work session.
-   Notify the incident to your laboratory safety officer via the [ticketing system](https://service.uni.lu/sp?id=sc_cat_item&sys_id=4343cc0ddbb070505c72ef3c0b961932&sysparm_category=cca7f2c1db683c905c72ef3c0b961940).


## Large spill in a BSC (more than few milliliters or more complicated spill situations)

The main challenge in this case is also to keep benefiting from the protection offered by the BSC while avoiding to export any contamination outside the cabinet. In case the spill would not be contained in the BSC, priority should be given to treating the part of the spill that lays outside the cabinet while keeping the BSC running.

-   Stop activities. Keep the BSC running and keep applying the rules for safe work in a BSC.
-   If your gloves have been exposed to contamination, change gloves. Remove the gloves carefully without spreading the contamination.
-   Close all open containers.
-  Inform your coworkers about the spill and look for assistance. If you don't feel comfortable with dealing with the spill, call the 5555. If you feel comfortable to manage the spill ask for the support of one of your colleague. 
-   Cover the spilled liquids with absorbing paper.
-   Pour gently the absorbing paper with 70% ethanol in order to have it soaked or other disinfectant depending on the biological agent in use.
-   Let the disinfecting solution act for 10 minutes, leaving all contaminated material within the BSC.
-   In the meantime, wash hands with soap and prepare other materials that would be needed (e.g., scoop, scraper, forceps, sharp safety box...).
-   With new gloves on, clean up the BSC and remove all debris and the absorbing material in a waste bag located inside the BSC, then discard the bag from the BSC and plce it in the external waste bag or container for infectious waste. The second person can help in holding the waste bags and providing any missing material.
-   Spray gently the exposed surfaces inside the BSC with 70% ethanol.
-   Wipe surfaces and sprayed objects with absorbing paper.
-   Change gloves.
-   Proceed to a final decontamination of the BSC with the disinfectant that is used in routine.
-   Resume or terminate work session.
-   Notify the incident to your laboratory safety officer via the [ticketing system](https://service.uni.lu/sp?id=sc_cat_item&sys_id=4343cc0ddbb070505c72ef3c0b961932&sysparm_category=cca7f2c1db683c905c72ef3c0b961940).




