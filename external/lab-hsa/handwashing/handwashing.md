---
layout: page
permalink: /external/lab-hsa/handwashing/
shortcut: lab:handwashing
redirect_from:
  - /cards/lab-hsa:handwashing
  - /external/cards/lab-hsa:handwashing
  - /lab/handwashing
  - /external/external/lab-hsa/handwashing/
  - /cards/lab:handwashing
  - /external/cards/lab:handwashing
---
# Handwashing

The procedure aims to describe the good practices in handwashing before and after laboratory manipulations.

## Description

1.	Handwashing is performed before doing any experiment in the lab as well as before exiting the laboratory.
2.	A good handwashing is essential to avoid contamination and must last at least 40 seconds.
3.	Consider the sink, including the faucet, as contaminated and avoid direct contact with them.
4.	Wet your hands with water.
5.	Dispense a dose of soap in the palm of your hand.
6.	Rub your hands palm to palm.
7.	Rub your right palm over left dorsum with interlaced fingers and vice versa.
8.	Rub palm to palm with fingers interlaced.
9.	Rub the back of your fingers to the opposing palms with fingers interlocked.
10.	Perform a rotational rubbing of the left thumb clasped in the right palm and vice versa.
11.	Perform a rotational rubbing backwards and forwards with clasped fingers of the right hand in the left palm and vice versa.
12.	Rinse your hands with water.
13.	Dry your hands thoroughly with a single use towel.
14.	Use the towel to turn off the faucet

<div align="center">
<img src="img/handwashing_img_1.png">
</div>

