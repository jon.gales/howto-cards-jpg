---
layout: page
permalink: /external/lab-hsa/spill/
shortcut: lab:spill
redirect_from:
  - /cards/lab-hsa:spill
  - /external/cards/lab-hsa:spill
  - /lab/spill
  - /external/external/lab-hsa/spill/
  - /cards/lab:spill
  - /external/cards/lab:spill
---

# Spill in a laboratory

Chemical or biological spills may happen. First of all, stay calm, each situation is different, but following the below instructions will enable you to face any situation.

During the emergency phase (first phase) you have to ensure your and your colleagues' protection.
Prevent aggravation if possible (recap flasks, shut down power or heat source).

In case somebody has been exposed to chemicals (skin or mucosa) rinse with **Diphoterine** as soon as possible.
To know more about the usage of Diphoterine, consult the dedicated card <a href="{{ '/lab-hsa/diphoterine/' | relative_url }}">here</a>.

In case of contact with biological reagents, rinse with water for 15min. You can use the eye shower of the lab/corridor.

Look for assitance to evacuate the laboratories and to take care of any injured people. To ensure the labs are evacuated, the lab technicians and the room responsible can also help you. 

Contact the **5555, always!** People from the 5555 will come to help you and will contact the safety officer (from your mobile phone use the number +352 46 66 44 5555). 
During working hours, you can also contact the safety officer to save time. 

In case of injured people: You are allowed to contact the 112 before contacting the 5555. Anyway, the 5555 will contact the 112 to directly coordinate with them.

Additionally, in case of any contamination with chemical compounds (by contact, inhalation, ingestion) check section 4 of the SDS (safety data sheet) of the product, referring to the first aids measures. 
If a victim is sent to the hospital, he should take a copy of the SDS to be provided to the medical staff.

Leave the area for 30min to allow aerosols to settle down and for the air of the laboratory to be renewed. During this time, prepare the second phase: the spill management.


## First phase: emergency measures in summary

The first thing to do is to follow the **SPILL** sequence:

<div align="center">
<img src="img/img1.png" width="500">
</div>

You can find the signalisation material needed to lock the aera and the spill kit to manage the spill at the safety points.

<div align="center">
<img src="img/img2.png" width="700">
</div>

When you are done with the emergency measures, you can start the **spill management**. 

## Second phase: the spill management

After the laboratory has been evacuated during the first phase, the second phase can start.

**Only wetlab people who have followed the spill training (theory + practical session) can manage the spill.** 

- Wait **30min** (time for aerosols to settle down and the air in the lab to be renewed) before entering again the laboratory. During this time, prepare the spill intervention, outside of the laboratory. 
- It is mandatory to be **2 persons** to manage the spill, never act alone. If you are alone in the lab, call the 5555, they will help you in the spill management. 
- If you do not feel confident, do not start the spill management, **ask for help** to an experienced colleague or the safety officer. 
- Prepare the **waste containers** and the **personal protective equipment**.  
- Think how to remove the PPE without contaminating yourself. 
- In any case, **report** the incident afterwards for follow up.

Here below, you will find illustrations of interventions for chemical and biological spills.

### Chemical Spill

1. Wait 30min between the spill and the intervention. 
2. Wear the appropriate PPE (refer to the safety datasheet).
3. Plan your work and your sequence of operations to avoid contaminating yourself.
4. Contain the spill with Trivorex. Trivorex contains a pH indicator. It should turn yellow once the chemical products are trapped, if not, add more Trivorex on the spill until it turns yellow.
5. Sprinkle to cover the entire spill and wait for 10min.
6. Remove with the shovel from the spill kit and discard it in a plastic bag that will be discarded in the blue bin with yellow cover (solids and liquids contaminated with biologicals and chemicals).

<div align="center">
<img src="img/img3.png">
</div>

### Biological Spill

1. Wait 30min between the spill and the intervention.
2. Wear the appropriate PPE.
3. Plan your work and your sequence of operations to avoid contaminating yourself.
4. For larger spill (>10mL): Prepare the HAZ tab fresh diluation (4 tablets in 1 litre of water)
5. Cover the spill with absorbent paper.

<div align="center">
<img src="img/img7.png" width="700">
</div>

6. Disinfect. The area of disinfection should be twice as big as the size of the spill.
    - For small spill (~10mL), use the disinfectant from the lab.
    - For larger spill, use the disinfectant from the spill kit.

<div align="center">
<img src="img/img5.png" width="700">
</div>

7. Respect the contact time of 10min for disinfection.
8. Collect the absorbent paper in the yellow bag from the spill kit and discard it in the blue bin with yellow cover (solids and liquids contaminated with biologicals and chemicals).

<div align="center">
<img src="img/img6.png" width="700">
</div>
