---
layout: landing
title: Home
order: 1
---

{% comment %}
The Bioinformatics Core and Lab Support teams assist researchers from the Luxembourg Centre for Systems Biomedicine ([LCSB](https://wwwen.uni.lu/lcsb)) with the organization, management, and curation of research data through its R3 initiative.
The How-to cards are intended to provide practical guidance in implementing Data Management, Data Protection, IT setup, lab support, and others.
{% endcomment %}

<script>
    window.cards = [
       {
            "title": "General",
            "caption": "I need information on LCSB operations",
            "icon": "<i class=\"large material-icons\">sticky_note_2</i>",
            "link": "{{ '/cards#general' | relative_url }}"
        },
        {
            "title": "On&Off boarding",
            "caption": "A new colleague joined or my current colleague will leave. What do I have to do?",
            "icon": "<i class=\"large material-icons\">compare_arrows</i>",
            "link": "{{ '/cards#on-offboarding' | relative_url }}"
        },
        {
            "title": "Access",
            "caption": "How can I get access to systems and infrastructure?",
            "icon": "<i class=\"large material-icons\">login</i>",
            "link": "{{ '/cards#access' | relative_url }}"
        },
        {
            "title": "Contribute",
            "caption": "I want to share my knowledge and expertise. How?",
            "icon": "<i class=\"large material-icons\">add_circle</i>",
            "link": "{{ '/cards#contribute' | relative_url }}"
        },
        {
            "title": "Backup",
            "caption": "Guidelines regarding backing up your data",
            "icon": "<i class=\"large material-icons\">backup</i>",
            "link": "{{ '/cards#backup' | relative_url }}"
        },
        {
            "title": "Exchange channels",
            "caption": "I am looking to share data or information",
            "icon": "<i class=\"large material-icons\">multiple_stop</i>",
            "link": "{{ '/cards#exchange-channels' | relative_url }}"
        },
        {
            "title": "Integrity",
            "caption": "I want to ensure that my work and research is compliant and reusable",
            "icon": "<i class=\"large material-icons\">vpn_key</i>",
            "link": "{{ '/cards#integrity' | relative_url }}"
        },
        {
            "title": "Lab",
            "caption": "I need information on wetlab equipment and procedures",
            "icon": "<i class=\"large material-icons\">science</i>",
            "link": "{{ '/cards#lab' | relative_url }}"
        },
        {
            "title": "Publication",
            "caption": "I want to publish my article",
            "icon": "<i class=\"large material-icons\">menu_book</i>",
            "link": "{{ '/cards#publication' | relative_url }}"
        }
    ];

    document.addEventListener('DOMContentLoaded', function() {
        window.start_cards();
    }, false);

</script>